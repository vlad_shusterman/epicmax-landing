import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Layout from '@/components/Layout'
import Works from '@/components/Works'
import Contact from '@/components/Contact'
import About from '@/components/About'
import NewsletterSubscription from '@/components/NewsletterSubscription'
import CasePage from '@/components/CasePage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Layout,
      redirect: {name: 'Home'},
      children: [
        {
          path: 'vuestic',
          redirect: 'works'
        },
        {
          name: 'Home',
          path: 'home',
          component: Home
        },
        {
          name: 'Works',
          path: 'works',
          component: Works
        },
        {
          name: 'About',
          path: 'about',
          component: About
        },
        {
          name: 'Contact',
          path: 'contact',
          component: Contact
        },
        {
          name: 'Newsletter',
          path: 'newsletter',
          component: NewsletterSubscription
        },
        {
          name: 'CasePage',
          path: 'case-page',
          component: CasePage
        }
      ]
    }
  ]
})
